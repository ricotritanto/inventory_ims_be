const Sequelize = require('sequelize')
module.exports = function(sequelize, DataTypes) {
	return sequelize.define('purchase_details', {
		id: {
			autoIncrement: true,
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
        purchase_id:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
        product_id:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
		quantity: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		unitcost: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
        total: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
		created_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
		},
		updated_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
		},
		deleted_at: {
			type: DataTypes.DATE,
			allowNull: true
		}
	}, {
		sequelize,
		tableName: 'purchase_details',
		timestamps: false,
		indexes: [
			{
				name: 'PRIMARY',
				unique: true,
				using: 'BTREE',
				fields: [
					{ name: 'id' },
				]
			},
            {
				name: 'purchase_details_FK',
				using: 'BTREE',
				fields: [
					{ name: 'purchase_id' },
				]
			},
            {
				name: 'purchase_details_products_FK',
				using: 'BTREE',
				fields: [
					{ name: 'product_id' },
				]
			},
		]
	})
}
