var DataTypes = require('sequelize').DataTypes
var _users = require('./users')
var _userRoles = require('./user_roles')
var _roles = require('./roles')
var _categories = require('./categories')
var _units = require('./units')
var _customers = require('./customers')
var _suppliers = require('./suppliers')
var _products = require('./products')
var _purchases = require('./purchases')
var _purchase_details = require('./purchase_details')
var _orders = require('./orders')
var _orderDetails = require('./order_details')
var _invoices = require('./invoices')

function initModels(sequelize) {
	var users = _users(sequelize, DataTypes)
    var userRoles = _userRoles(sequelize, DataTypes)
    var roles = _roles(sequelize, DataTypes)
	var categories = _categories(sequelize, DataTypes)
	var units = _units(sequelize, DataTypes)
	var customers = _customers(sequelize, DataTypes)
	var suppliers = _suppliers(sequelize, DataTypes)
	var products = _products(sequelize, DataTypes)
	var purchases = _purchases(sequelize, DataTypes)
	var purchase_details = _purchase_details(sequelize, DataTypes)
    var orders = _orders(sequelize, DataTypes)
    var orderDetails = _orderDetails(sequelize, DataTypes)
	var invoices = _invoices(sequelize, DataTypes)

	// associate user with roles
	users.belongsToMany(roles, {as:'roles', through: userRoles, foreignKey:'id_user'})
	roles.belongsToMany(users, {as:'users', through: userRoles,  foreignKey:'roleId'})

    users.hasMany(orders, {as:'orders', foreignKey:'user_id'})
	orders.belongsTo(users, {as:'users', foreignKey:'user_id'})

    users.hasMany(purchases, {as:'purchases', foreignKey:'user_id'})
	purchases.belongsTo(users, {as:'users', foreignKey:'user_id'})

    categories.hasMany(products, {as:'products', foreignKey:'category_id'})
	products.belongsTo(categories, {as:'categories', foreignKey:'category_id'})

    units.hasMany(products, {as:'products', foreignKey:'unit_id'})
	products.belongsTo(units, {as:'units', foreignKey:'unit_id'})

    suppliers.hasMany(purchases, {as:'purchases', foreignKey:'supplier_id'})
	purchases.belongsTo(suppliers, {as:'suppliers', foreignKey:'supplier_id'})

	customers.hasMany(orders, {as:'orders', foreignKey:'customer_id'})
	orders.belongsTo(customers, {as:'customers', foreignKey:'customer_id'})


	orders.hasMany(orderDetails, {as:'orderDetails', foreignKey:'order_id'})
	orderDetails.belongsTo(orders, {as:'orders',foreignKey:'order_id'})

    products.hasMany(orderDetails, {as:'orderDetails', foreignKey:'product_id'})
	orderDetails.belongsTo(products, {as:'products',foreignKey:'product_id'})

    invoices.hasMany(orderDetails, {as:'orderDetails', foreignKey:'invoice_id'})
	orderDetails.belongsTo(invoices, {as:'invoices',foreignKey:'invoice_id'})

    purchases.hasMany(purchase_details, {as:'purchase_details', foreignKey:'purchase_id'})
	purchase_details.belongsTo(purchases, {as:'purchases',foreignKey:'purchase_id'})

    products.hasMany(purchase_details, {as:'purchase_details', foreignKey:'product_id'})
	purchase_details.belongsTo(products, {as:'products',foreignKey:'product_id'})

	return {
		users,
		roles,
		userRoles,
        categories,
        units,
		customers,
		suppliers,
		products,
        purchases,
        purchase_details,
        orders,
		orderDetails,
		invoices,
	}
}
module.exports = initModels
// module.exports.initModels = initModels
// module.exports.default = initModels
