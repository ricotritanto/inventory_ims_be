const Sequelize = require('sequelize')
module.exports = function(sequelize, DataTypes) {
	return sequelize.define('suppliers', {
		id: {
			autoIncrement: true,
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
		name: {
			type: DataTypes.STRING(150),
			allowNull: false
		},
		email: {
			type: DataTypes.STRING(150),
			allowNull: true
		},
		address: {
			type: DataTypes.STRING(150),
			allowNull: true
		},
		phone: {
			type: DataTypes.STRING(50),
			allowNull: true
		},
		type: {
			type: DataTypes.STRING(50),
			allowNull: true
		},
		bank_name: {
			type: DataTypes.STRING(50),
			allowNull: true
		},
		account_holder: {
			type: DataTypes.STRING(50),
			allowNull: true
		},
		account_number: {
			type: DataTypes.STRING(50),
			allowNull: true
		},
        shopname: {
			type: DataTypes.STRING(50),
			allowNull: true
		},
		created_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
		},
		updated_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
		},
		deleted_at: {
			type: DataTypes.DATE,
			allowNull: true
		}
	}, {
		sequelize,
		tableName: 'suppliers',
		timestamps: false,
		indexes: [
			{
				name: 'PRIMARY',
				unique: true,
				using: 'BTREE',
				fields: [
					{ name: 'id' },
				]
			},
		]
	})
}
