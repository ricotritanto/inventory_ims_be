const Sequelize = require('sequelize')
module.exports = function(sequelize, DataTypes) {
	return sequelize.define('purchases', {
		id: {
			autoIncrement: true,
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
        user_id:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
        supplier_id:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
		purchase_date: {
			type: DataTypes.STRING(50),
			allowNull: false
		},
		purchase_no: {
			type: DataTypes.STRING(10),
			allowNull: true
		},
        purchase_status: {
            type: DataTypes.STRING(50),
            allowNull: true,
        },
		created_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
		},
		updated_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
		},
		deleted_at: {
			type: DataTypes.DATE,
			allowNull: true
		}
	}, {
		sequelize,
		tableName: 'purchases',
		timestamps: false,
		indexes: [
			{
				name: 'PRIMARY',
				unique: true,
				using: 'BTREE',
				fields: [
					{ name: 'id' },
				]
			},
            {
				name: 'purchases_user_FK',
				using: 'BTREE',
				fields: [
					{ name: 'user_id' },
				]
			},
            {
				name: 'purchases_supplier_FK',
				using: 'BTREE',
				fields: [
					{ name: 'supplier_id' },
				]
			},
		]
	})
}
