const Sequelize = require('sequelize')
module.exports = function(sequelize, DataTypes) {
	return sequelize.define('order_details', {
		id: {
			autoIncrement: true,
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
        order_id:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
        product_id:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
		quantity: {
			type: DataTypes.INTEGER,
			allowNull: false
		},
		unitcost: {
			type: DataTypes.INTEGER,
			allowNull: true
		},
        total: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        invoice_id: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
		created_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
		},
		updated_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
		},
		deleted_at: {
			type: DataTypes.DATE,
			allowNull: true
		}
	}, {
		sequelize,
		tableName: 'order_details',
		timestamps: false,
		indexes: [
			{
				name: 'PRIMARY',
				unique: true,
				using: 'BTREE',
				fields: [
					{ name: 'id' },
				]
			},
            {
				name: 'orderDetails_products_FK',
				using: 'BTREE',
				fields: [
					{ name: 'product_id' },
				]
			},
            {
				name: 'orderDetails_invoices_FK',
				using: 'BTREE',
				fields: [
					{ name: 'invoice_id' },
				]
			},
            {
				name: 'orderDetails_orders_FK',
				using: 'BTREE',
				fields: [
					{ name: 'order_id' },
				]
			},
		]
	})
}
