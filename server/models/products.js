const Sequelize = require('sequelize')
module.exports = function(sequelize, DataTypes) {
	return sequelize.define('products', {
		id: {
			autoIncrement: true,
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
        category_id:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
        unit_id:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
		product_name: {
			type: DataTypes.STRING(150),
			allowNull: false
		},
		product_code: {
			type: DataTypes.STRING(10),
			allowNull: false
		},
		serial_number: {
			type: DataTypes.STRING(25),
			allowNull: true
		},
        buying_price: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
        selling_price: {
            type: DataTypes.INTEGER,
            allowNull: true,
        },
		image_name: {
            type: DataTypes.STRING,
            allowNull: true,
        },
		url: {
            type: DataTypes.STRING,
            allowNull: true,
        },
		created_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
		},
		updated_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
		},
		deleted_at: {
			type: DataTypes.DATE,
			allowNull: true
		}
	}, {
		sequelize,
		tableName: 'products',
		timestamps: false,
		indexes: [
			{
				name: 'PRIMARY',
				unique: true,
				using: 'BTREE',
				fields: [
					{ name: 'id' },
				]
			},
		]
	})
}
