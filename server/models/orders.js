const Sequelize = require('sequelize')
module.exports = function(sequelize, DataTypes) {
	return sequelize.define('orders', {
		id: {
			autoIncrement: true,
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true
		},
        user_id:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
        customer_id:{
            type: DataTypes.INTEGER,
            allowNull: false
        },
		order_date: {
			type: DataTypes.STRING(10),
			allowNull: false
		},
		order_status: {
			type: DataTypes.STRING(10),
			allowNull: true
		},
        total_products: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        sub_total: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },   
        total: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        payment_type: {
            type: DataTypes.STRING(11),
            allowNull: true,
        },
        pay: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
        due: {
            type: DataTypes.INTEGER(11),
            allowNull: true,
        },
		created_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
		},
		updated_at: {
			type: DataTypes.DATE,
			allowNull: false,
			defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
		},
		deleted_at: {
			type: DataTypes.DATE,
			allowNull: true
		}
	}, {
		sequelize,
		tableName: 'orders',
		timestamps: false,
		indexes: [
			{
				name: 'PRIMARY',
				unique: true,
				using: 'BTREE',
				fields: [
					{ name: 'id' },
				]
			},
            {
				name: 'orders_customers_FK',
				using: 'BTREE',
				fields: [
					{ name: 'customer_id' },
				]
			},
            {
				name: 'orders_users_FK',
				using: 'BTREE',
				fields: [
					{ name: 'user_id' },
				]
			},
		]
	})
}
