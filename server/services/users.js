'use strict'
require('dotenv').config()
const usersRepo = require('../repository/userRepo')
const sequelize = require('sequelize')
const { check, validationResult } = require('express-validator')

const config = require('../config/auth.config')
var jwt = require('jsonwebtoken')
var bcrypt = require('bcryptjs')

const create = async (req) => {
    const fieldsToCheck = ['name', 'email', 'username']

    const validationPromises = fieldsToCheck.map((field) => {
        return check(field, `${field} is required`).notEmpty().run(req)
    })

    await Promise.all(validationPromises)
    const result = validationResult(req)
    if (!result.isEmpty()) {
        const errorMessages = result.array().map((error) => `${error.param} ${error.msg}`)
        return {
            status: 422,
            message: errorMessages.join(', '),
        }
    }

    const checkIsExistCode = await usersRepo.findOne(req)

    if (checkIsExistCode) {
        return {
            status: 400,
            message: `${checkIsExistCode.username} is already`,
        }
    }

    try {
        await usersRepo.create(req)
        return {
            status: 201,
            message: 'success created data',
        }
    } catch (error) {
        return {
            status: 500,
            message: 'something went wrong',
        }
    }
}

const update = async(req)=>{
	const fieldsToCheck = ['name', 'email', 'username']
    const validationPromises = fieldsToCheck.map((field) => {
        return check(field).notEmpty().withMessage(`${field} is required`).run(req)
    })

    await Promise.all(validationPromises)
    const result = validationResult(req)
    if (!result.isEmpty()) {
        const errorMessages = result.array().map(error => error.msg).join(', ')
        return {
            status: 422,
            message: errorMessages,
        }
    }

	const existingUser = await usersRepo.find(req)
	if(!existingUser) return{
		status:400,
		message: 'customers name not found'
	}
	if (req.body.username && req.body.username !== existingUser.username) {
		const existingRecord = await usersRepo.findData(1, 10, {}, [{ username: req.body.username }]);
		if (existingRecord.count) {
			return {
				status: 400,
				message: 'username already exists!'
			};
		}
	}
	
	try {
		req.body.updated_at = sequelize.fn('NOW')
		await usersRepo.updateById(req)
		return {
			status:201,
			message: 'success updating data'
		}
	} catch (error) {
		return {
			status: 500,
			message: 'something went wrong'
		}
	}
}

const getAll = async(req) =>{
	const data = await usersRepo.getAll(req,req.per_page,req.page)
	const result = {
		count: data.count,
		page:parseFloat(data.page),
		per_page: parseFloat(data.per_page),
		items: data.rows
	}
	return {
		status: 200,
		result
	}
}

const deleteData = async(req) =>{
	try {
		await usersRepo.delete(req)
		return {
			status:201,
			message: 'success delete data'
		}
	} catch (error) {
		return {
			status: 500,
			message: 'something went wrong'
		}
	}
}

const signin = async(req)=>{
	const user = await usersRepo.signin(req)
	// try {
	if(!user)
		return {
			status:400,
			result:{
				message: 'user not found.'
			}
		}
	var passwordIsValid = bcrypt.compareSync(
		req.body.password,
		user.password
	)
	// jika password tidak true
	if(!passwordIsValid){
		return {
			status:401,
			result:{
				'x-access-token': null,
				message: 'Invalid Password!'
			}
		}
	}

	var token = jwt.sign({id:user.id}, config.secret,{
		expiresIn: 600 //24 jam
	})
	try{
		const getRoles = await usersRepo.getRole(user)
		return {
			status:200,
			id:user.id,
			username: user.username,
			email: user.email,
			roles: getRoles,
			'x-access-token': token
		}
	}catch(err) {
		return {
			status: 500,
			message:err.message
		}
	}
    
}

const getRoles = async(data)=>{
	const roles = await usersRepo.getAllRole(data,data.per_page,data.page)
	const result = {
		count: roles.count,
		page:parseFloat(data.page),
		per_page: parseFloat(data.per_page),
		items: roles.rows,
	}
	return {
		status: 200,
		result
	}
}
const userById = async(req)=>{
	// console.log(req)
	try{		
		const result = await usersRepo.getUserById(req)
		return {
			status: 201,
			message: 'successfully',
			data:result
		}
		
	}catch(error) {
		return {
			status:500,
			message: error.message
		}
	}
}

const changePassword = async(req)=>{
	await check('password', 'password is required').notEmpty().run(req)
	await check('password', 'Password must be  8 characters !! ').isLength({ min: 8 }).run(req)
	const result = validationResult(req)
	if (!result.isEmpty()) {
		return {
			status:422,
			message: result.errors.map(msg=>msg.msg)
		}
	}
	const dataUser = await usersRepo.find(req)
	if(!dataUser) return{
		status:400,
		message: 'data not found'
	}

	try{
		req.body.updated_at = sequelize.fn('NOW')
		const result = await usersRepo.updatePassword(req)
		if(result){
			return {
				status: 201,
				message: 'Update Password Successfully',
			}
		}
		
	}catch(error) {
		return {
			status:500,
			message: error.message
		}
	}
}



module.exports = {
	create,
	update,
	getAll,
	deleteData,
	signin,
	getRoles,
	userById,
	changePassword
}