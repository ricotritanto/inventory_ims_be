'use strict'
require('dotenv').config()
const purchasesRepo = require('../repository/purchasesRepo')
const sequelize = require('sequelize')
const { check, validationResult } = require('express-validator')


const create = async(req)=>{
	// const fieldsToCheck = ['user_id','supplier_id','purchase_date', 'purchase_no', 'purchase_status', 'product_id', 'quantity', 'unitcost', 'total']
	const fieldsToCheck = ['user_id','supplier_id','purchase_date', 'purchase_no', 'purchase_status']
    const validationPromises = fieldsToCheck.map((field) => {
        return check(field).notEmpty().withMessage(`${field} is required`).run(req)
    })

	 // Validasi array products
	 if (!Array.isArray(req.body.products) || req.body.products.length === 0) {
        return {
            status: 422,
            message: 'products array is required and should not be empty',
        };
    }

    // Validasi setiap product dalam array
    req.body.products.forEach((product, index) => {
        validationPromises.push(
            check(`products[${index}].product_id`).notEmpty().withMessage(`products[${index}].product is required`).run(req),
            check(`products[${index}].quantity`).notEmpty().withMessage(`products[${index}].quantity is required`).isNumeric().withMessage(`products[${index}].quantity should be a number`).run(req),
            check(`products[${index}].unitcost`).notEmpty().withMessage(`products[${index}].unitcost is required`).isNumeric().withMessage(`products[${index}].unitcost should be a number`).run(req),
            check(`products[${index}].total`).notEmpty().withMessage(`products[${index}].total is required`).isNumeric().withMessage(`products[${index}].total should be a number`).run(req)
        );
    });

    await Promise.all(validationPromises)
    const result = validationResult(req)
    if (!result.isEmpty()) {
        const errorMessages = result.array().map(error => error.msg).join(', ')
        return {
            status: 422,
            message: errorMessages,
        }
    }

	const checkIsExistCode = await purchasesRepo.findOne(req)
	if(checkIsExistCode)
		return {
			status:400,
			message: req.body.purchase_no+' is already'
		}
	try {
		await purchasesRepo.createPurchases(req)
		return {
			status: 201,
			message: 'success created data'
		}
	} catch (error) {
		return {
			status: 500,
			message: 'something went wrong'
		}
	}
	
}

const update = async(req)=>{
	const fieldsToCheck = ['user_id'];
    const validationPromises = fieldsToCheck.map((field) => {
        return check(field).notEmpty().withMessage(`${field} is required`).run(req);
    });

    // Validasi array products
    if (!Array.isArray(req.body.products) || req.body.products.length === 0) {
        return {
            status: 422,
            message: 'products array is required and should not be empty',
        };
    }

    // Validasi setiap product dalam array
    req.body.products.forEach((product, index) => {
        validationPromises.push(
            check(`products[${index}].product_id`).notEmpty().withMessage(`products[${index}].product is required`).run(req),
            check(`products[${index}].quantity`).notEmpty().withMessage(`products[${index}].quantity is required`).isNumeric().withMessage(`products[${index}].quantity should be a number`).run(req),
            check(`products[${index}].unitcost`).notEmpty().withMessage(`products[${index}].unitcost is required`).isNumeric().withMessage(`products[${index}].unitcost should be a number`).run(req),
            check(`products[${index}].total`).notEmpty().withMessage(`products[${index}].total is required`).isNumeric().withMessage(`products[${index}].total should be a number`).run(req)
        );
    });

    await Promise.all(validationPromises);
    const result = validationResult(req);
    if (!result.isEmpty()) {
        const errorMessages = result.array().map(error => error.msg).join(', ');
        return {
            status: 422,
            message: errorMessages,
        };
    }

    try {
        const purchaseId = req.params.id;
        const userId = req.body.user_id;

        // Update data purchase
        const updatedPurchase = await purchasesRepo.updateById(req, purchaseId, userId);

        if (updatedPurchase) {
            return {
                status: 200,
                message: 'success updated data',
            };
        } else {
            return {
                status: 404,
                message: 'purchase not found',
            };
        }
    } catch (error) {
        console.error(error);
        return {
            status: 500,
            message: 'something went wrong',
        };
    }
}

const getAll = async(data) =>{
	const purchases = await purchasesRepo.getAll(data,data.per_page,data.page)
	const result = {
		count: purchases.count,
		page:parseFloat(data.page),
		per_page: parseFloat(data.per_page),
		items: purchases.rows
	}
	return {
		status: 200,
		result
	}
}

const deletePurchases = async(req)=>{
	try {
		await purchasesRepo.deletePurchases(req)
		return {
			status:201,
			message: 'success updating data'
		}
	} catch (error) {
        console.log(error)
		return {
			status: 500,
			message: 'something went wrong'
		}
	}
}

const getID = async (req)=>{
	const purchases = await purchasesRepo.findID(req)
	const result = {
		count: purchases.count,
		page:parseFloat(req.page),
		per_page: parseFloat(req.per_page),
		items: purchases.rows
	}
	return {
		status: 200,
		result
	}
}

module.exports = {
	create,
	update,
	getAll,
	deletePurchases,
	getID
}