'use strict'
require('dotenv').config()
const ordersRepo = require('../repository/ordersRepo')
const sequelize = require('sequelize')
const { check, validationResult } = require('express-validator')


const create = async(req)=>{
	
	const fieldsToCheck = ['user_id','customer_id','order_date', 'total_products', 'sub_total', 'payment_type', 'pay', 
                            'order_status', 'invoice_no', 'invoice_date']
    const validationPromises = fieldsToCheck.map((field) => {
        return check(field).notEmpty().withMessage(`${field} is required`).run(req)
    })

	if (!Array.isArray(req.body.products) || req.body.products.length === 0) {
        return {
            status: 422,
            message: 'products array is required and should not be empty',
        };
    }

	 // Validasi setiap product dalam array
	 req.body.products.forEach((product, index) => {
        validationPromises.push(
            check(`products[${index}].product_id`).notEmpty().withMessage(`products[${index}].product is required`).run(req),
            check(`products[${index}].quantity`).notEmpty().withMessage(`products[${index}].quantity is required`).isNumeric().withMessage(`products[${index}].quantity should be a number`).run(req),
            check(`products[${index}].unitcost`).notEmpty().withMessage(`products[${index}].unitcost is required`).isNumeric().withMessage(`products[${index}].unitcost should be a number`).run(req),
            check(`products[${index}].total`).notEmpty().withMessage(`products[${index}].total is required`).isNumeric().withMessage(`products[${index}].total should be a number`).run(req)
        );
    });

    await Promise.all(validationPromises)
    const result = validationResult(req)
    if (!result.isEmpty()) {
        const errorMessages = result.array().map(error => error.msg).join(', ')
        return {
            status: 422,
            message: errorMessages,
        }
    }

	const checkIsExistCode = await ordersRepo.findOne(req)
	if(checkIsExistCode)
		return {
			status:400,
			message: req.body.invoice_no+' is already'
		}
	try {
		await ordersRepo.createOrders(req)
		return {
			status: 201,
			message: 'success created data'
		}
	} catch (error) {
		return {
			status: 500,
			message: 'something went wrong'
		}
	}
	
}

const update = async(req)=>{
    const fieldsToCheck = ['user_id','customer_id','order_date', 'total_products', 'sub_total', 'payment_type', 'pay', 
                            'order_status', 'sub_total']
	const validationPromises = fieldsToCheck.map((field) => {
		return check(field).notEmpty().withMessage(`${field} is required`).run(req)
	})

	if (!Array.isArray(req.body.products) || req.body.products.length === 0) {
		return {
			status: 422,
			message: 'products array is required and should not be empty',
		};
	}

	 // Validasi setiap product dalam array
	 req.body.products.forEach((product, index) => {
        validationPromises.push(
            check(`products[${index}].product_id`).notEmpty().withMessage(`products[${index}].product is required`).run(req),
            check(`products[${index}].quantity`).notEmpty().withMessage(`products[${index}].quantity is required`).isNumeric().withMessage(`products[${index}].quantity should be a number`).run(req),
            check(`products[${index}].unitcost`).notEmpty().withMessage(`products[${index}].unitcost is required`).isNumeric().withMessage(`products[${index}].unitcost should be a number`).run(req),
            check(`products[${index}].total`).notEmpty().withMessage(`products[${index}].total is required`).isNumeric().withMessage(`products[${index}].total should be a number`).run(req)
        );
    });

    await Promise.all(validationPromises)
    const result = validationResult(req)
    if (!result.isEmpty()) {
        const errorMessages = result.array().map(error => error.msg).join(', ')
        return {
            status: 422,
            message: errorMessages,
        }
    }

	try {
        const orderID = req.params.id;
        const userID = req.body.user_id;

        // Update data purchase
        const updatedOrder = await ordersRepo.updateById(req, orderID, userID);

        if (updatedOrder) {
            return {
                status: 200,
                message: 'success updated data',
            };
        } else {
            return {
                status: 404,
                message: 'order not found',
            };
        }
    } catch (error) {
        console.error(error);
        return {
            status: 500,
            message: 'something went wrong',
        };
    }

	
}

const getAll = async(data) =>{
	const orders = await ordersRepo.getAll(data,data.per_page,data.page)
	const result = {
		count: orders.count,
		page:parseFloat(data.page),
		per_page: parseFloat(data.per_page),
		items: orders.rows
	}
	return {
		status: 200,
		result
	}	
}

const deleteOrders = async(req)=>{
	try {
		await ordersRepo.deleteOrders(req)
		return {
			status:201,
			message: 'success updating data'
		}
	} catch (error) { console.log(error)
		return {
			status: 500,
			// message: 'something went wrong'
			message:error
		}
	}
}

const getID = async (req)=>{
	const orders = await ordersRepo.findID(req)
	const result = {
		count: orders.count,
		page:parseFloat(req.page),
		per_page: parseFloat(req.per_page),
		items: orders.rows
	}
	return {
		status: 200,
		result
	}
}

module.exports = {
	create,
	update,
	getAll,
	deleteOrders,
	getID
}