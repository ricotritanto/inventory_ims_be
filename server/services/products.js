'use strict'
require('dotenv').config()
const productRepo = require('../repository/productsRepo')
const sequelize = require('sequelize')
const { check, validationResult } = require('express-validator')
const path = require('path');
const md5 = require('md5');
const fs = require('fs');

const deleteImageFile = (filePath) => {
    try {
        if (fs.existsSync(filePath)) {
            fs.unlinkSync(filePath);
            console.log('Image deleted successfully');
        } else {
            console.log('File not found, unable to delete image');
        }
    } catch (err) {
        console.error('Error deleting image:', err);
    }
};


const create = async(req, res)=>{
	const fieldsToCheck = ['unit','category','product_name', 'product_code']
    const validationPromises = fieldsToCheck.map((field) => {
        return check(field).notEmpty().withMessage(`${field} is required`).run(req.body)
    })

    await Promise.all(validationPromises)

	if (req.file === null || req.file === undefined || !req.file || Object.keys(req.file).length === 0 ){
		return {
			status: 400,
			message: 'no file uploaded'
		}
	}
	const fileRelativePath = path.relative(path.join(__dirname, '..', '..', 'public'), req.file.path);
    const fileUrl = `http://localhost:3002/${fileRelativePath}`;
	const { unit, category, product_name, product_code, serial_number, buying_price, selling_price  } = req.body;
	const productData = {
		unit, category, product_name, product_code, serial_number, buying_price, selling_price,
		image_name: req.file.fileName, url: req.file.path
	}
	const checkIsExistCode = await productRepo.findOne(productData)
	
	if(checkIsExistCode){
		if (productData.product_image) {
			await deleteImageFile(productData.product_image);
		}
		return {
			status:400,
			message: productData.product_name+' is already'
		}
	}
	try {
		await productRepo.createProducts(productData)
		return {
			status: 201,
			message: 'success created data'
		}
	} catch (error) {
		console.log(error)
		if (uploadPath && fs.existsSync(uploadPath)) {
            fs.unlinkSync(uploadPath);
        }
		return {
			status: 500,
			message: 'something went wrong'
		}
	}
	
}

const update = async(req)=>{
	const fieldsToCheck = ['unit','category','product_name', 'product_code']
    const validationPromises = fieldsToCheck.map((field) => {
        return check(field).notEmpty().withMessage(`${field} is required`).run(req)
    })

    await Promise.all(validationPromises)
    const result = validationResult(req)
    if (!result.isEmpty()) {
        const errorMessages = result.array().map(error => error.msg).join(', ')
        return {
            status: 422,
            message: errorMessages,
        }
    }
	const { unit, category, product_name, product_code, serial_number, buying_price, selling_price  } = req.body;
	let image_name, url;
	const productName = await productRepo.find(req)
	if(!productName) return{
		status:400,
		message: 'product not found'
	}
	if(req.body.product_name != undefined && req.body.product_name != productName.product_name){
		const existingRecord = await productRepo.findData(1, 10, {}, [{product_name: req.body.product_name}])
		if (existingRecord.count) {
			return {
				status: 400,
				message: 'Product name already exist!'
			}
		}
	}
	// Periksa apakah ada file gambar yang diunggah
	if (req.file === null || req.file === undefined || !req.file || Object.keys(req.file).length === 0 ) {
		image_name = productName.image_name;
		url = productName.url;
	}
	else {
		deleteImageFile(productName.url);
		image_name = req.file.fileName;
		url = req.file.path;
	}
	const productData = {
		unit, category, product_name, product_code, serial_number, buying_price, selling_price,
		image_name, url
	}
	try {
		req.body.updated_at = sequelize.fn('NOW')
		const productId = req.params.id; 
		await productRepo.updateById(productId, productData)
		return {
			status:201,
			message: 'success updating data'
		}
	} catch (error) {
		console.log(error)
		return {
			status: 500,
			message: 'something went wrong'
		}
	}
}

const getAll = async(data) =>{
	const products = await productRepo.getAllProducts(data,data.per_page,data.page)
	const result = {
		count: products.count,
		page:parseFloat(data.page),
		per_page: parseFloat(data.per_page),
		items: products.rows
	}
	return {
		status: 200,
		result
	}
}

const deleteProduct = async (req) => {
    try {
        const product = await productRepo.getProductById(req); // Mengambil data produk yang akan dihapus
        if (!product) {
            return {
                status: 404,
                message: 'Product not found'
            };
        }

        const deletedRows = await productRepo.deleteProducts(req);
        if (deletedRows > 0) {
            // Hapus file gambar dari penyimpanan lokal
            const imagePath = product.url; // Ubah ini dengan jalur tempat gambar disimpan
            fs.unlinkSync(imagePath);
            return {
                status: 200,
                message: 'Product deleted successfully'
            };
        } else {
            return {
                status: 404,
                message: 'Product not found'
            };
        }
    } catch (error) {
        console.error(error);
        return {
            status: 500,
            message: 'Something went wrong'
        };
    }
}

module.exports = {
	create,
	update,
	getAll,
	deleteProduct,
}