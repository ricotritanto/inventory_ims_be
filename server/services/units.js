'use strict'
require('dotenv').config()
const unitsRepo = require('../repository/unitsRepo')
const sequelize = require('sequelize')
const { check, validationResult } = require('express-validator')

const create = async(req)=>{
	await check('name', 'name is required').notEmpty().run(req)
	const result = validationResult(req)
	if (!result.isEmpty()) {
		return {
			status:422,
			message:'unit name is required'
		}
	}

	const checkIsExistCode = await unitsRepo.findOne(req)
	if(checkIsExistCode)
		return {
			status:400,
			message: req.body.name+' is already'
		}
	try {
		await unitsRepo.createUnits(req)
		return {
			status:201,
			message: 'success created data'
		}
	} catch (error) {
		return {
			status: 500,
			message: 'something went wrong'
		}
	}
}

const update = async(req)=>{
	await check('name', 'name is required').notEmpty().run(req)
	const result = validationResult(req)
	if (!result.isEmpty()) {
		return {
			status:422,
			message:'unit name is required'
		}
	}
	const unitName = await unitsRepo.find(req)
	if(!unitName) return{
		status:400,
		message: 'unit name not found'
	}
	if(req.body.name != undefined && req.body.name != unitName.unit_name){
		const existingRecord = await unitsRepo.findData(1, 10, {}, [{unit_name: req.body.name}])
		if (existingRecord.count) {
			return {
				status: 400,
				message: 'unit name already exist!'
			}
		}
	}
	try {
		req.body.updated_at = sequelize.fn('NOW')
		await unitsRepo.updateById(req)
		return {
			status:201,
			message: 'success updating data'
		}
	} catch (error) {
		return {
			status: 500,
			message: 'something went wrong'
		}
	}
}

const getAll = async(data) =>{
	const units = await unitsRepo.getAllUnits(data,data.per_page,data.page)
	const result = {
		count: units.count,
		page:parseFloat(data.page),
		per_page: parseFloat(data.per_page),
		items: units.rows
	}
	return {
		status: 200,
		result
	}
}

const deleteData = async(req) =>{
	try {
		await unitsRepo.deleteUnits(req)
		return {
			status:201,
			message: 'success delete data'
		}
	} catch (error) {
		return {
			status: 500,
			message: 'something went wrong'
		}
	}
}

module.exports = {
	create,
	update,
	getAll,
	deleteData
}