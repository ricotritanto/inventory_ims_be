'use strict'
require('dotenv').config()
const suppliersRepo = require('../repository/suppliersRepo')
const sequelize = require('sequelize')
const { check, validationResult } = require('express-validator')

const create = async (req) => {
    const fieldsToCheck = ['name', 'email', 'phone', 'address', 'type', 'bank_name', 'account_holder', 'account_number','shopname']

    const validationPromises = fieldsToCheck.map((field) => {
        return check(field, `${field} is required`).notEmpty().run(req)
    })

    await Promise.all(validationPromises)
    const result = validationResult(req)
    if (!result.isEmpty()) {
        const errorMessages = result.array().map((error) => `${error.param} ${error.msg}`)
        return {
            status: 422,
            message: errorMessages.join(', '),
        }
    }

    const checkIsExistCode = await suppliersRepo.findOne(req)

    if (checkIsExistCode) {
        return {
            status: 400,
            message: `${checkIsExistCode.name} is already`,
        }
    }

    try {
        await suppliersRepo.createSuppliers(req)
        return {
            status: 201,
            message: 'success created data',
        }
    } catch (error) {
        return {
            status: 500,
            message: 'something went wrong',
        }
    }
}

const update = async(req)=>{
	const fieldsToCheck = ['name', 'email', 'phone', 'address', 'type', 'bank_name', 'account_holder', 'account_number', 'shopname']
    const validationPromises = fieldsToCheck.map((field) => {
        return check(field).notEmpty().withMessage(`${field} is required`).run(req)
    })

    await Promise.all(validationPromises)
    const result = validationResult(req)
    if (!result.isEmpty()) {
        const errorMessages = result.array().map(error => error.msg).join(', ')
        return {
            status: 422,
            message: errorMessages,
        }
    }

	const suppliersName = await suppliersRepo.find(req)
	if(!suppliersName) return{
		status:400,
		message: 'supplier name not found'
	}
	if(req.body.name != undefined && req.body.name != suppliersName.name){
		const existingRecord = await suppliersRepo.findData(1, 10, {}, [{name: req.body.name}])
		if (existingRecord.count) {
			return {
				status: 400,
				message: 'suppliers name already exist!'
			}
		}
	}
	try {
		req.body.updated_at = sequelize.fn('NOW')
		await suppliersRepo.updateById(req)
		return {
			status:201,
			message: 'success updating data'
		}
	} catch (error) {
		return {
			status: 500,
			message: 'something went wrong'
		}
	}
}

const getAll = async(req) =>{
	const data = await suppliersRepo.getAll(req,req.per_page,req.page)
	const result = {
		count: data.count,
		page:parseFloat(data.page),
		per_page: parseFloat(data.per_page),
		items: data.rows
	}
	return {
		status: 200,
		result
	}
}

const deleteData = async(req) =>{
	try {
		await suppliersRepo.deleteSuppliers(req)
		return {
			status:201,
			message: 'success delete data'
		}
	} catch (error) {
		return {
			status: 500,
			message: 'something went wrong'
		}
	}
}

module.exports = {
	create,
	update,
	getAll,
	deleteData
}