'user strict'

const usersServices = require('../services/users')

const signup = async(req,res, next) =>{
	return usersServices.create(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err.message))
}

const signin = async(req,res, next) =>{
	return usersServices.signin(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const getUser = async(req, res, next) =>{
	return usersServices.getAll(req.query)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const getRole = async(req, res, next) =>{
	return usersServices.getRoles(req.query)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}
const getById = async(req,res,next)=>{
	return usersServices.userById(req.query)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const updateUser = async(req,res,next) =>{
	return usersServices.update(req)
		.then(result=> res.status(result.status).send(result))
		.catch(err=>next(err)) 
}

const deleteUser = async(req,res,next)=>{
	return usersServices.deleteUser(req)
		.then(result=>res.status(result.status).send(result))
		.catch(err => next(err))
}

const changePassword = async(req,res,next)=>{
	return usersServices.changePassword(req)
		.then(result=>res.status(result.status).send(result))
		.catch(err => next(err))
}


module.exports = {
	// getAll,
	signin,
	signup,
	getUser,
	getRole,
	getById,
	updateUser,
	deleteUser,
	changePassword
}