'use strict'
const ordersServices = require('../services/orders')

const getAll = async(req, res, next) =>{
	return ordersServices.getAll(req.query)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const deleteOrders = async(req, res,next)=>{
	return ordersServices.deleteOrders(req.params.id)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const updateOrders = async(req,res,next) =>{
	return ordersServices.update(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const createOrders = async(req,res, next) =>{
	return ordersServices.create(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const getID = async (req, res, next) => {
	return ordersServices.getID(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

module.exports = {
	getAll,
	deleteOrders,
	updateOrders,
	createOrders,
	getID
}