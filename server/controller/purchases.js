'use strict'
const purchasesService = require('../services/purchases')

const getAll = async(req, res, next) =>{
	return purchasesService.getAll(req.query)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const deletePurchases = async(req, res,next)=>{
	return purchasesService.deletePurchases(req.params)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const updatePurchases = async(req,res,next) =>{
	return purchasesService.update(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const createPurchases = async(req,res, next) =>{
	try {
		const result = await purchasesService.create(req);
		res.status(result.status).send(result);
	} catch (err) {
		next(err);
	}
}

const getID = async (req, res, next) => {
	return purchasesService.getID(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

module.exports = {
	getAll,
	deletePurchases,
	updatePurchases,
	createPurchases,
	getID
}