'use strict'
const suppliersServices = require('../services/suppliers')

const getAll = async(req, res, next) =>{
	return suppliersServices.getAll(req.query)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const deleteSuppliers = async(req, res,next)=>{
	return suppliersServices.deleteData(req.params.id)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const updateSuppliers = async(req,res,next) =>{
	return suppliersServices.update(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const createSuppliers = async(req,res, next) =>{
	return suppliersServices.create(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

module.exports = {
	getAll,
	deleteSuppliers,
	updateSuppliers,
	createSuppliers
}