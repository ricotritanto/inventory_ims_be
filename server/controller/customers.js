'use strict'
const customersServices = require('../services/customers')

const getAll = async(req, res, next) =>{
	return customersServices.getAll(req.query)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const deleteCustomers = async(req, res,next)=>{
	return customersServices.deleteData(req.params.id)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const updateCustomers = async(req,res,next) =>{
	return customersServices.update(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const createCustomers = async(req,res, next) =>{
	return customersServices.create(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

module.exports = {
	getAll,
	deleteCustomers,
	updateCustomers,
	createCustomers
}