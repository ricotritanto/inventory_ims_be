'use strict'
const categoriesServices = require('../services/categories')

const getAll = async(req, res, next) =>{
	return categoriesServices.getAll(req.query)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const deleteCategories = async(req, res,next)=>{
	return categoriesServices.deleteData(req.params.id)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const updateCategories = async(req,res,next) =>{
	return categoriesServices.update(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const createCategories = async(req,res, next) =>{
	return categoriesServices.create(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

module.exports = {
	getAll,
	deleteCategories,
	updateCategories,
	createCategories
}