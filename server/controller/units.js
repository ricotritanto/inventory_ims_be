'use strict'
const unitServices = require('../services/units')

const getAll = async(req, res, next) =>{
	return unitServices.getAll(req.query)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const deleteUnits = async(req, res,next)=>{
	return unitServices.deleteData(req.params.id)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const updateUnits = async(req,res,next) =>{
	return unitServices.update(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

const createUnits = async(req,res, next) =>{
	return unitServices.create(req)
		.then(result => res.status(result.status).send(result))
		.catch(err => next(err))
}

module.exports = {
	getAll,
	deleteUnits,
	updateUnits,
	createUnits
}