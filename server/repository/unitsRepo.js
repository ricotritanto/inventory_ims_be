/* eslint-disable no-mixed-spaces-and-tabs */
'use strict'
const {models} = require('../models')
const {Op } = require('sequelize')

const insertBulk = async (data) => {
	return	await models.units.bulkCreate(data)
		
}

const getAllUnits = async(data,limit =10, offset = 0)=>{
	const { name } = data
	var condition = name ? { unit_name: { [Op.like]: `%${name}%` } } : null
	return await models.units.findAndCountAll({ 
		where: condition,
		limit:parseInt(limit),
		offset: (offset <= 1) ? 0 : ((offset - 1) * parseFloat(limit)),
		order:[['unit_name', 'DESC']]
	 })
		.catch(ex => {
			throw ex
		})
}

const deleteUnits = async(req)=>{
	return await models.units.destroy({
		where:{id:req}
	})
}

const updateById = async(req)=>{
	return await models.units.update({
		unit_name:req.body.name.toLowerCase(),
	}
	,{
		where:{id:req.params.id}
	})
}

const createUnits = async(req)=>{
	return await models.units.create({
		unit_name:req.body.name.toLowerCase(),
	})
}

const findData = async(unitsName,req, search = {},specificWhere = []) =>{
	let where = {
		deleted_at: null,
	}
	if (Object.keys(search).length > 0) {
		where[Op.or]= []
		for (const [key, value] of Object.entries(search)) {
		  const src = {}
		  src[key] = {
				[Op.like]: '%'+value+'%'
		  }
		  where[Op.or].push(src)
		}
  
		Object.assign(where)
	  }
	  if(specificWhere.length > 0) {
		specificWhere.map(v => Object.assign(where, v))
	  }
	  const cond = {
		where,
	  }
	  return await models.units.findAndCountAll(cond)
		.catch(ex => {
			throw ex
		})
}

const find = async(req) =>{
	return await models.units.findOne({
		where: {
			id:req.params.id,
			deleted_at: null
		}
	})
}

const findOne = async(req) =>{
	return await models.units.findOne({
		where: {
			unit_name:req.body.name.toLowerCase(),
			deleted_at: null
		}
	})
}

const findUnits = async(data) =>{
	return await models.units.findOne({
		where: {
			unit_name : data.units.toLowerCase()
		},
		attributes:['id'],
		raw:true,
		plain:true
	})
}


module.exports = {
	insertBulk,
	getAllUnits,
	deleteUnits,
	updateById,
	createUnits,
	findOne,
	findData,
	find,
	findUnits
}