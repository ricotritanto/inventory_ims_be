/* eslint-disable no-mixed-spaces-and-tabs */
'use strict'
const {models, db} = require('../models')
const {Op } = require('sequelize')

var bcrypt = require('bcryptjs')

const insertBulk = async (data) => {
	return	await models.users.bulkCreate(data)
		
}

const getAll = async(data,limit =10, offset = 0)=>{
	const { username, email } = data
    const conditions = {}
    if (username) {
        conditions.username = { [Op.like]: `%${username}%` };
    }
    if (email) {
        conditions.email = { [Op.like]: `%${email}%` };
    }
	return await models.users.findAndCountAll({ 
		where: conditions,
		limit:parseInt(limit),
		offset: (offset <= 1) ? 0 : ((offset - 1) * parseFloat(limit)),
		include: {
			model: models.roles,
			as: 'roles',
			required: false,
		},
		order:[['username', 'DESC']]
	 })
		.catch(ex => {
			throw ex
		})
}

const deleteUsers = async(req)=>{
	return await models.users.destroy({
		where:{id:req}
	})
}

const updateById = async(req)=>{
	try {
		const user = await db.sequelize.transaction(async (t) =>{
			await models.users.update({
				username:req.body.username.toLowerCase(),
				email:req.body.email,
				role:req.body.roles,
				password:bcrypt.hashSync(req.body.password, 8),
				updated_at:req.body.updated_at
			},{
				where:{id:req.params.id}
			}, {transaction: t})
			await models.userRoles.update({
				roleId:req.body.roles,
				id_user:req.params.id,
				updated_at:req.body.updated_at
			},{
				where:{id_user:req.params.id}
			}, {transaction: t})
		})
		return user
	} catch (error) {
		console.log(error)
		return error
	}
}

const create = async(req)=>{
	try {
		const user = await db.sequelize.transaction(async (t) =>{
			const result = await models.users.create({
                name: req.body.name,
				username: req.body.username,
				email: req.body.email,
				password: bcrypt.hashSync(req.body.password, 8)
			},{transaction: t})
			const idUser = JSON.stringify(result.id)
			await models.userRoles.create({
				roleId:req.body.roles,
				id_user:idUser
			}, {transaction: t})            
		})
		return user
	} catch (error) {
		console.log(error)
	}
}
const findData = async (username, req, search = {}, specificWhere = []) => {
    const where = {
        deleted_at: null,
    };

    if (Object.keys(search).length > 0) {
        where[Op.or] = [];
        for (const [key, value] of Object.entries(search)) {
            const src = {};
            src[key] = {
                [Op.like]: `%${value}%`,
            };
            where[Op.or].push(src);
        }
    }

    if (specificWhere.length > 0) {
        specificWhere.map((v) => Object.assign(where, v));
    }

    const cond = {
        where,
    };

    return await models.users.findAndCountAll(cond).catch((ex) => {
        throw ex;
    });
}

const find = async(req) =>{
	return await models.users.findOne({
		where: {
			id:req.params.id,
			deleted_at: null
		}
	})
}

const findOne = async(req) =>{
	return await models.users.findOne({
		where: {
			username:req.body.username.toLowerCase(),
			deleted_at: null
		}
	})
}

const findUsers = async(data) =>{
	return await models.users.findOne({
		where: {
			username : data.users.username.toLowerCase()
		},
		attributes:['id'],
		raw:true,
		plain:true
	})
}

const signin = async (req) =>{
	return await models.users.findOne({
		where:{
			email:req.body.email,
			username: req.body.username
		}
	})
	
}

const checkUser = async(req)=>{
	return await models.users.findOne({
		where:{
			sname: req.body.username
		}
	})
}

const checkEmail = async(req)=>{
	return await models.users.findOne({
		where:{
			email: req.body.email
		}
	})
}

const getRole = async(user)=>{
	return await models.users.findAndCountAll({
		where: {
			id:user.id,	
			deleted_at: null
		},
		include:[{
			model: models.roles,
			through:{attributes: []},
			as:'roles'
		}]
	})
}

const getAllRole = async(data,limit =10, offset = 0)=>{
	const { role } = data
	var condition = role ? { username: { [Op.like]: `%${role}%` } } : null
	return await models.roles.findAndCountAll({ 
		where: condition,
		limit:parseInt(limit),
		offset: (offset <= 1) ? 0 : ((offset - 1) * parseFloat(limit)),
		order:[['role', 'DESC']]
	})
		.catch(ex => {
			throw ex
		})
}

const getUserById = async(req) =>{
	return await models.users.findById({
		where: {
			id:req.params.id,
			deleted_at: null
		}
	})

}

const updatePassword = async(req)=>{
	return await models.users.update({
		password:bcrypt.hashSync(req.body.password, 8),
		updated_at:req.body.updated_at
	}
	,{
		where:{id:req.params.id}
	})
}
module.exports = {
	insertBulk,
	getAll,
	deleteUsers,
	updateById,
	create,
	findOne,
	findData,
	find,
	findUsers,
	signin,
	checkEmail,
	checkUser,
	getRole,
	getAllRole,
	getUserById,
	updatePassword
}