/* eslint-disable no-mixed-spaces-and-tabs */
'use strict'
const {models, db}= require('../models')
const {Op } = require('sequelize')


const moment = require('moment');

const insertBulk = async (data) => {
	return await models.orders.bulkCreate(data)
}

const getAll = async(data, limit = 10, offset = 0) => {
    const { invoice_no, user, customer, start_date, end_date, order_status } = data;
    let whereConditions = {};
    let whereOrderDetails = {};
    let whereUsers = {};
    let whereCustomers = {};
    let whereInvoices = {};
    
    const formattedStartDate = start_date ? moment(start_date, 'DD-MM-YYYY').format('YYYY-MM-DD') : null;
    const formattedEndDate = end_date ? moment(end_date, 'DD-MM-YYYY').format('YYYY-MM-DD') : null;

    // Filter by order date range (start_date & end_date)
    if (start_date && end_date) {
        whereConditions.order_date = { [Op.between]: [formattedStartDate, formattedEndDate] };
    }

    // Filter by customer
    if (customer) {
        whereCustomers.id = customer;  // assuming customer is customer_id
    }

    // Filter by user
    if (user) {
        whereUsers.id = user;  // assuming user is user_id
    }

    if(order_status){
        whereConditions.order_status = {[Op.like]: `%${order_status}%`}
    }

	if (invoice_no) {
        const invoice = await models.invoices.findOne({
            where: {
                [Op.and]: [
                    { invoice_no: { [Op.startsWith]: invoice_no } },
                    db.sequelize.where(db.sequelize.fn('LENGTH', db.sequelize.col('invoice_no')), invoice_no.length)
                ]
            },
            attributes: ['id', 'invoice_no']
        });

        if (invoice) {
            whereOrderDetails.invoice_id = invoice.id;  // Use invoice_id for filtering
        } else {
            // If no matching invoice_no found, return empty result
            return {
                status: 200,
                result: {
                    count: 0,
                    page: parseFloat(data.page),
                    per_page: parseFloat(data.per_page),
                    items: []
                }
            };
        }
    }

    return await models.orders.findAndCountAll({
        where: whereConditions,
        limit: parseInt(limit),
        offset: (offset <= 1) ? 0 : ((offset - 1) * parseFloat(limit)),
        distinct: true,
        include: [
            {
                model: models.orderDetails,
                as: 'orderDetails',
                required: true,
                where: whereOrderDetails,
                attributes: ['id', 'order_id', 'product_id', 'invoice_id', 'quantity', 'unitcost', 'total'],
                include: {
                    model: models.invoices,
                    as: 'invoices',
                    required: false,
                    where: whereInvoices,
                    attributes: ['id', 'invoice_no', 'invoice_date', 'note']
                }
            },
            {
                model: models.users,
                as: 'users',
                required: true,
                where: whereUsers,
                attributes: ['id', 'name']
            },
            {
                model: models.customers,
                as: 'customers',
                required: true,
                where: whereCustomers,
                attributes: ['id', 'name']
            }
        ],
        order: [['order_date', 'DESC']]
    })
    .catch(ex => {
        throw ex;
    });
}


const deleteOrders = async(req, res)=>{
	try {
        return await db.sequelize.transaction(async (t)=>{
			const orderDetails = await models.orderDetails.findAll({
                where: { order_id: req },
                attributes: ['invoice_id'],  // Ambil invoice_id dari orderDetails
                transaction: t
            });

            await models.orderDetails.destroy({
                where: { order_id: req}, transaction: t
            })

            const deleteOrder = await models.orders.destroy({
                where: { id: req}, transaction: t
            })

			const invoiceIds = orderDetails.map(detail => detail.invoice_id);
            await models.invoices.destroy({
                where: { id: invoiceIds }, transaction: t
            });
			
            return deleteOrder       
		})
    }catch(error){
        await transaction.rollback() 
        console.log(error)

      res.status(500).json({ message: 'Error deleting Orders.' });
    }
}
const updateById = async (req, orderID, userID) => {
    try {
        return await db.sequelize.transaction(async (t) => {
            // Update order data
            const updatedOrder = await models.orders.update({
                user_id: userID,
                customer_id: req.body.customer_id,
                order_date: req.body.order_date,
                total_products: req.body.total_products,
                sub_total: req.body.sub_total,
                payment_type: req.body.payment_type,
                pay: req.body.pay,
                order_status: req.body.order_status
            }, {
                where: { id: orderID },
                transaction: t
            });

			const existingInvoice = await models.invoices.findOne({
                where: { id: req.body.invoice_id },
                transaction: t
            });

            if (!existingInvoice) {
                throw new Error('Invoice not found');
            }
            
            // update note di invoices
            await models.invoices.update({
                note: req.body.note,
            },{
                where: { id: req.body.invoice_id},
                transaction: t
            })             

            // Update or recreate orderDetails
            await models.orderDetails.destroy({ where: { order_id: orderID }, transaction: t });
            const orderDetails = req.body.products.map(product => ({
                product_id: product.product_id,
                quantity: product.quantity,
                unitcost: product.unitcost,
                total: product.total,
                order_id: orderID,
                invoice_id: existingInvoice.id 
            }));

            await models.orderDetails.bulkCreate(orderDetails, { transaction: t });

            return updatedOrder;
        });
    } catch (error) {
        console.error(error);
        throw error;
    }
};


const createOrders = async(req)=>{
    try {
		const orders = await db.sequelize.transaction(async (t) =>{
			const result = await models.orders.create({               
                user_id: req.body.user_id,
                customer_id: req.body.customer_id,
                order_date: req.body.order_date,
                order_status: req.body.order_status,
                total_products: req.body.total_products,
                sub_total: req.body.sub_total,
                // total : req.body.sub_total,
                payment_type : req.body.payment_type,
                pay : req.body.pay,
                due : req.body.due
			},{transaction: t})
			const orderID = JSON.stringify(result.id)
            const invoice = await models.invoices.create({		
                invoice_no: req.body.invoice_no,
                invoice_date: req.body.invoice_date,
				note: req.body.note
			}, {transaction: t})   
			const invoiceID = JSON.stringify(invoice.id)   
			const orderDetails = req.body.products.map(product =>({
				product_id: product.product_id,
                quantity: product.quantity,
                unitcost: product.unitcost,
				order_id: orderID,
                invoice_id: invoiceID,
				total: product.total
			}))        
			await models.orderDetails.bulkCreate(orderDetails, { transaction: t }); 
			return result
		})
		return orders
	} catch (error) {
		console.log(error)
	}
}

const generateInvoiceNo = () => {
    // Mendapatkan tanggal saat ini
    const currentDate = new Date()
  
    // Format tanggal, bulan, tahun, jam, menit, dan detik
    const formattedDate = `${currentDate.getDate()}${currentDate.getMonth() + 1}${currentDate.getFullYear()}${currentDate.getHours()}${currentDate.getMinutes()}${currentDate.getSeconds()}`
  
    // Gabungkan dengan format invoice (contoh: "inv-20220101120030")
    const newInvoiceNo = `inv-${formattedDate}`
  
    return newInvoiceNo;
}
  
const find = async(req) =>{
	return await models.orders.findOne({
		where: {
			id:req.params.id,
			deleted_at: null
		}
	})
}

const findData = async(page, perpage,search = {},specificWhere = []) =>{
	let where = {
		deleted_at: null,
	}
	if (Object.keys(search).length > 0) {
		where[Op.or]= []
		for (const [key, value] of Object.entries(search)) {
		  const src = {}
		  src[key] = {
				[Op.like]: '%'+value+'%'
		  }
		  where[Op.or].push(src)
		}
  
		Object.assign(where)
	  }
	  if(specificWhere.length > 0) {
		specificWhere.map(v => Object.assign(where, v))
	  }
	  const cond = {
		where,
	  }
	  return await models.orders.findAndCountAll(cond)
		.catch(ex => {
			throw ex
		})
}
const findOne = async(req) =>{
	return await models.invoices.findOne({
		where: {invoice_no:req.body.invoice_no},
		deleted_at:null
	})
}


const findOrders = async(data) =>{
	return await models.orders.findOne({
		where: {
			purchase_no : data.orders.order_no
		},
		attributes:['id'],
		raw:true,
		// plain:true
	})
}

const findID = async (req) => {
	return await models.orders.findAndCountAll({ 
		where: {id : req.params.id},
		// limit:parseInt(limit),
		// offset: (offset <= 1) ? 0 : ((offset - 1) * parseFloat(limit)),
        include:[
			{
				model: models.orderDetails,
				as:'orderDetails',
				required:true,
				attributes:['id', 'order_id','product_id', 'quantity', 'unitcost', 'total','invoice_id'],
                include: [
					{
						model: models.products,
						as:'products',
						required:false,
						// where: whereProducts,
						attributes:['id','product_name','product_code','serial_number'],
					}	,
					{			
						model: models.invoices,
						as:'invoices',
						required:true,
						attribute:['invoice_no','invoice_date']
				
					}	
				]		
			},
            {
				model: models.users,
				as:'users',
				required:true,
				// where: whereUsers,
				attributes:['id','name']
				
			},
            {
				model: models.customers,
				as:'customers',
				required:true,
				attributes:['id','name','bank_name','address', 'phone', 'email']
				
			}
		],
		order:[['order_date', 'DESC']]
	 })
		.catch(ex => {
			throw ex
		})
}

module.exports = {
	insertBulk,
	getAll,
	deleteOrders,
	updateById,
	createOrders,
	findOne,
	find,
	findData,
	findOrders,
    generateInvoiceNo,
	findID
}