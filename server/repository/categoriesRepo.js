/* eslint-disable no-mixed-spaces-and-tabs */
'use strict'
const {models} = require('../models')
const {Op } = require('sequelize')

const insertBulk = async (data) => {
	return	await models.categories.bulkCreate(data)
		
}

const getAllUnits = async(data,limit =10, offset = 0)=>{
	const { name } = data
	var condition = name ? { category_name: { [Op.like]: `%${name}%` } } : null
	return await models.categories.findAndCountAll({ 
		where: condition,
		limit:parseInt(limit),
		offset: (offset <= 1) ? 0 : ((offset - 1) * parseFloat(limit)),
		order:[['category_name', 'DESC']]
	 })
		.catch(ex => {
			throw ex
		})
}

const deleteCategory = async(req)=>{
	return await models.categories.destroy({
		where:{id:req}
	})
}

const updateById = async(req)=>{
	return await models.categories.update({
		category_name:req.body.name.toLowerCase(),
	}
	,{
		where:{id:req.params.id}
	})
}

const createUnits = async(req)=>{
	return await models.categories.create({
		category_name:req.body.name.toLowerCase(),
	})
}
const findData = async (categoriesName, req, search = {}, specificWhere = []) => {
    const where = {
        deleted_at: null,
    };

    if (Object.keys(search).length > 0) {
        where[Op.or] = [];
        for (const [key, value] of Object.entries(search)) {
            const src = {};
            src[key] = {
                [Op.like]: `%${value}%`,
            };
            where[Op.or].push(src);
        }
    }

    if (specificWhere.length > 0) {
        specificWhere.map((v) => Object.assign(where, v));
    }

    const cond = {
        where,
    };

    return await models.categories.findAndCountAll(cond).catch((ex) => {
        throw ex;
    });
}

const find = async(req) =>{
	return await models.categories.findOne({
		where: {
			id:req.params.id,
			deleted_at: null
		}
	})
}

const findOne = async(req) =>{
	return await models.categories.findOne({
		where: {
			category_name:req.body.name.toLowerCase(),
			deleted_at: null
		}
	})
}

const findCategories = async(data) =>{
	return await models.categories.findOne({
		where: {
			category_name : data.categories.toLowerCase()
		},
		attributes:['id'],
		raw:true,
		plain:true
	})
}


module.exports = {
	insertBulk,
	getAllUnits,
	deleteCategory,
	updateById,
	createUnits,
	findOne,
	findData,
	find,
	findCategories
}