/* eslint-disable no-mixed-spaces-and-tabs */
'use strict'
const {models} = require('../models')
const {Op } = require('sequelize')

const insertBulk = async (data) => {
	return	await models.suppliers.bulkCreate(data)
		
}

const getAll = async(data,limit =10, offset = 0)=>{
	const { name, account_number, shopname } = data
    const conditions = {}
    if (name) {
        conditions.name = { [Op.like]: `%${name}%` };
    }
    if (account_number) {
        conditions.account_number = { [Op.like]: `%${account_number}%` };
    }
    if (shopname) {
        conditions.shopname = { [Op.like]: `%${shopname}%` };
    }
	return await models.suppliers.findAndCountAll({ 
		where: conditions,
		limit:parseInt(limit),
		offset: (offset <= 1) ? 0 : ((offset - 1) * parseFloat(limit)),
		order:[['name', 'DESC']]
	 })
		.catch(ex => {
			throw ex
		})
}

const deleteSuppliers = async(req)=>{
	return await models.suppliers.destroy({
		where:{id:req}
	})
}

const updateById = async(req)=>{
	return await models.suppliers.update({
		name:req.body.name.toLowerCase(),
        email:req.body.email.toLowerCase(),
        phone:req.body.phone,
        address:req.body.address.toLowerCase(),
        type:req.body.type.toLowerCase(),
        bank_name:req.body.bank_name.toLowerCase(),
        account_holder:req.body.account_holder.toLowerCase(),
        account_number:req.body.account_number,
        shopname:req.body.shopname,
	}
	,{
		where:{id:req.params.id}
	})
}

const createSuppliers = async(req)=>{
	return await models.suppliers.create({
		name:req.body.name.toLowerCase(),
        email:req.body.email.toLowerCase(),
        phone:req.body.phone,
        address:req.body.address.toLowerCase(),
        type:req.body.type.toLowerCase(),
        bank_name:req.body.bank_name.toLowerCase(),
        account_holder:req.body.account_holder.toLowerCase(),
        account_number:req.body.account_number,
        shopname:req.body.shopname,

	})
}
const findData = async (suppliersName, req, search = {}, specificWhere = []) => {
    const where = {
        deleted_at: null,
    };

    if (Object.keys(search).length > 0) {
        where[Op.or] = [];
        for (const [key, value] of Object.entries(search)) {
            const src = {};
            src[key] = {
                [Op.like]: `%${value}%`,
            };
            where[Op.or].push(src);
        }
    }

    if (specificWhere.length > 0) {
        specificWhere.map((v) => Object.assign(where, v));
    }

    const cond = {
        where,
    };

    return await models.suppliers.findAndCountAll(cond).catch((ex) => {
        throw ex;
    });
}

const find = async(req) =>{
	return await models.suppliers.findOne({
		where: {
			id:req.params.id,
			deleted_at: null
		}
	})
}

const findOne = async(req) =>{
	return await models.suppliers.findOne({
		where: {
			name:req.body.name.toLowerCase(),
			deleted_at: null
		}
	})
}

const findSuppliers = async(data) =>{
	return await models.suppliers.findOne({
		where: {
			name : data.suppliers.toLowerCase()
		},
		attributes:['id'],
		raw:true,
		plain:true
	})
}


module.exports = {
	insertBulk,
	getAll,
	deleteSuppliers,
	updateById,
	createSuppliers,
	findOne,
	findData,
	find,
	findSuppliers
}