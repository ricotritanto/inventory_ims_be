/* eslint-disable no-mixed-spaces-and-tabs */
'use strict'
const {models, db}= require('../models')
const {Op } = require('sequelize')
const moment = require('moment');

const insertBulk = async (data) => {
	return await models.purchases.bulkCreate(data)
}

const getAll = async(data,limit =10, offset = 0)=>{
    const { purchase_no, user, supplier, start_date, end_date, product_name, purchase_status } = data;
    let whereConditions = {};
    let wherePurchaseDetails = {};
    let whereUsers = {};
    let whereSupplier = {};
    let whereProducts= {};
    
    const formattedStartDate = start_date ? moment(start_date, 'DD-MM-YYYY').format('YYYY-MM-DD') : null;
    const formattedEndDate = end_date ? moment(end_date, 'DD-MM-YYYY').format('YYYY-MM-DD') : null;
    // Filter by order date range (start_date & end_date)
    if (start_date && end_date) {
        whereConditions.purchase_date = { [Op.between]: [formattedStartDate, formattedEndDate] };
    }

    // Filter by customer
    if (supplier) {
        whereSupplier.id = supplier;  // assuming customer is customer_id
    }

    // Filter by user
    if (user) {
        whereUsers.id = user;  // assuming user is user_id
    }

    if (product_name) {
        whereConditions.product_name = { [Op.like]: `%${product_name}%` }
    }
    if (purchase_status) {
        whereConditions.purchase_status = { [Op.like]: `%${purchase_status}%` }
    }


	if (purchase_no) {
        const purchase = await models.purchases.findOne({
            where: {
                [Op.and]: [
                    { purchase_no: { [Op.startsWith]: purchase_no } },
                    db.sequelize.where(db.sequelize.fn('LENGTH', db.sequelize.col('purchase_no')), purchase_no.length)
                ]
            },
            attributes: ['id', 'purchase_no']
        });

        if (purchase) {
            wherePurchaseDetails.purchase_id = purchase.id;  // Use invoice_id for filtering
        } else {
            // If no matching invoice_no found, return empty result
            return {
                status: 200,
                result: {
                    count: 0,
                    page: parseFloat(data.page),
                    per_page: parseFloat(data.per_page),
                    items: []
                }
            };
        }
    }
	return await models.purchases.findAndCountAll({ 
		where: whereConditions,
		limit:parseInt(limit),
		offset: (offset <= 1) ? 0 : ((offset - 1) * parseFloat(limit)),
        distinct: true,
        include:[
			{
				model: models.purchase_details,
				as:'purchase_details',
				required:true,
				where: wherePurchaseDetails,
				attributes:['id', 'purchase_id','product_id', 'quantity', 'unitcost', 'total'],
                include: {
                    model: models.products,
                    as:'products',
                    required:false,
                    where: whereProducts,
                    attributes:['id','product_name'],
                }				
			},
            {
				model: models.users,
				as:'users',
				required:true,
				where: whereUsers,
				attributes:['id','name']
				
			},
            {
				model: models.suppliers,
				as:'suppliers',
				required:true,
				where: whereSupplier,
				attributes:['id','name']
				
			},
		],
		order:[['purchase_date', 'DESC']]
	 })
		.catch(ex => {
			throw ex
		})
}

const deletePurchases = async(req, res)=>{
    try {
        return await db.sequelize.transaction(async (t)=>{
            await models.purchase_details.destroy({
                where: { purchase_id: req.id}, transaction: t
            })

            const deletePurchase = await models.purchases.destroy({
                where: { id: req.id}, transaction: t
            })
            return deletePurchase
        })
    }catch(error){
        await transaction.rollback() 
        console.log(error)

      res.status(500).json({ message: 'Error deleting purchase.' });
    }
}

const updateById = async(req, purchaseId, userId) => {
	try {
        return await db.sequelize.transaction(async (t) => {
            // Update user_id di tabel purchases
            await models.purchases.update(
                { user_id: userId },
                { where: { id: purchaseId }, transaction: t }
            );

            // Ambil semua detail purchase yang ada berdasarkan purchase_id
            const existingDetails = await models.purchase_details.findAll({
                where: { purchase_id: purchaseId },
                transaction: t
            });

            // Update existing details atau tambahkan detail baru
            const updatedDetails = req.body.products.map(product => {
                const existingDetail = existingDetails.find(detail => detail.id === product.id);
                if (existingDetail) {
                    // Jika detail sudah ada, update datanya
                    return {
                        id: existingDetail.id,
                        product_id: product.product_id,
                        quantity: product.quantity,
                        unitcost: product.unitcost,
                        total: product.total,
                        purchase_id: purchaseId
                    };
                } else {
                    // Jika detail tidak ada, buat detail baru
                    return {
                        product_id: product.product_id,
                        quantity: product.quantity,
                        unitcost: product.unitcost,
                        total: product.total,
                        purchase_id: purchaseId
                    };
                }
            });

            // Hapus semua detail lama yang tidak ada dalam request baru
            const updatedDetailIds = updatedDetails.map(detail => detail.id).filter(id => id !== undefined);
            const detailsToDelete = existingDetails.filter(detail => !updatedDetailIds.includes(detail.id));
            if (detailsToDelete.length > 0) {
                await models.purchase_details.destroy({
                    where: { id: detailsToDelete.map(detail => detail.id) },
                    transaction: t
                });
            }

            // Bulk update or create details
            await Promise.all(updatedDetails.map(detail => {
                if (detail.id) {
                    // Update existing detail
                    return models.purchase_details.update(detail, {
                        where: { id: detail.id },
                        transaction: t
                    });
                } else {
                    // Create new detail
                    return models.purchase_details.create(detail, { transaction: t });
                }
            }));

            // Kembalikan hasil purchase yang telah diupdate
            const updatedPurchase = await models.purchases.findOne({
                where: { id: purchaseId },
                transaction: t
            });

            return updatedPurchase;
        });
    } catch (error) {
        console.error(error);
        throw new Error('Error updating purchase');
    }
};


const createPurchases = async(req)=>{
    try {
		const purchases = await db.sequelize.transaction(async (t) =>{
			const result = await models.purchases.create({               
                user_id: req.body.user_id,
                supplier_id: req.body.supplier_id,
                purchase_date: req.body.purchase_date,
                purchase_no: req.body.purchase_no,
                purchase_status: req.body.purchase_status,
			},{transaction: t})
			const purchaseID = JSON.stringify(result.id)
			const purchaseDetails = req.body.products.map(product => ({
                product_id: product.product_id,
                quantity: product.quantity,
                unitcost: product.unitcost,
                total: product.total,
                purchase_id: purchaseID
            }));
			await models.purchase_details.bulkCreate(purchaseDetails, { transaction: t });

            return result;
		})
		return purchases
	} catch (error) {
		console.log(error)
	}
}

const find = async(req) =>{
	return await models.purchases.findOne({
		where: {
			id:req.params.id,
			deleted_at: null
		}
	})
}

const findData = async(page, perpage,search = {},specificWhere = []) =>{
	let where = {
		deleted_at: null,
	}
	if (Object.keys(search).length > 0) {
		where[Op.or]= []
		for (const [key, value] of Object.entries(search)) {
		  const src = {}
		  src[key] = {
				[Op.like]: '%'+value+'%'
		  }
		  where[Op.or].push(src)
		}
  
		Object.assign(where)
	  }
	  if(specificWhere.length > 0) {
		specificWhere.map(v => Object.assign(where, v))
	  }
	  const cond = {
		where,
	  }
	  return await models.purchases.findAndCountAll(cond)
		.catch(ex => {
			throw ex
		})
}
const findOne = async(req) =>{
	return await models.purchases.findOne({
		where: {purchase_no:req.body.purchase_no},
		deleted_at:null
	})
}

const findID = async (req) => {
	return await models.purchases.findAndCountAll({ 
		where: {id : req.params.id},
		// limit:parseInt(limit),
		// offset: (offset <= 1) ? 0 : ((offset - 1) * parseFloat(limit)),
        include:[
			{
				model: models.purchase_details,
				as:'purchase_details',
				required:true,
				attributes:['id', 'purchase_id','product_id', 'quantity', 'unitcost', 'total'],
                include: {
                    model: models.products,
                    as:'products',
                    required:false,
                    // where: whereProducts,
                    attributes:['id','product_name','product_code','serial_number'],
                }				
			},
            {
				model: models.users,
				as:'users',
				required:true,
				// where: whereUsers,
				attributes:['id','name']
				
			},
            {
				model: models.suppliers,
				as:'suppliers',
				required:true,
				attributes:['id','name','address', 'email', 'phone']
				
			},
		],
		order:[['purchase_date', 'DESC']]
	 })
		.catch(ex => {
			throw ex
		})
}


const findPurchases = async(data) =>{
	return await models.purchases.findOne({
		where: {
			purchase_no : data.purchases.purchase_no
		},
		attributes:['id'],
		raw:true,
		// plain:true
	})
}

const generatePurchaseNo = () => {
    // Mendapatkan tanggal saat ini
    const currentDate = new Date()
  
    // Format tanggal, bulan, tahun, jam, menit, dan detik
    const formattedDate = `${currentDate.getDate()}${currentDate.getMonth() + 1}${currentDate.getFullYear()}${currentDate.getHours()}${currentDate.getMinutes()}${currentDate.getSeconds()}`
  
    // Gabungkan dengan format invoice (contoh: "inv-20220101120030")
    const newPurchaseNo = `pch-${formattedDate}`
  
    return newPurchaseNo
}

module.exports = {
	insertBulk,
	getAll,
	deletePurchases,
	updateById,
	createPurchases,
	findOne,
	find,
	findData,
	findPurchases,
    generatePurchaseNo,
	findID
}