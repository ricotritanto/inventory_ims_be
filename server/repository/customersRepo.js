/* eslint-disable no-mixed-spaces-and-tabs */
'use strict'
const {models} = require('../models')
const {Op } = require('sequelize')

const insertBulk = async (data) => {
	return	await models.customers.bulkCreate(data)
		
}

const getAllCustomers = async(data,limit =10, offset = 0)=>{
	const { name, account_number } = data
    const conditions = {}
    if (name) {
        conditions.name = { [Op.like]: `%${name}%` };
    }
    if (account_number) {
        conditions.account_number = { [Op.like]: `%${account_number}%` };
    }
	return await models.customers.findAndCountAll({ 
		where: conditions,
		limit:parseInt(limit),
		offset: (offset <= 1) ? 0 : ((offset - 1) * parseFloat(limit)),
		order:[['name', 'DESC']]
	 })
		.catch(ex => {
			throw ex
		})
}

const deleteCustomers = async(req)=>{
	return await models.customers.destroy({
		where:{id:req}
	})
}

const updateById = async(req)=>{
	return await models.customers.update({
		name:req.body.name.toLowerCase(),
        email:req.body.email.toLowerCase(),
        phone:req.body.phone,
        address:req.body.address.toLowerCase(),
        type:req.body.type.toLowerCase(),
        bank_name:req.body.bank_name.toLowerCase(),
        account_holder:req.body.account_holder.toLowerCase(),
        account_number:req.body.account_number,
        photo:req.body.photo,
	}
	,{
		where:{id:req.params.id}
	})
}

const createCustomers = async(req)=>{
	return await models.customers.create({
		name:req.body.name.toLowerCase(),
        email:req.body.email.toLowerCase(),
        phone:req.body.phone,
        address:req.body.address.toLowerCase(),
        type:req.body.type.toLowerCase(),
        bank_name:req.body.bank_name.toLowerCase(),
        account_holder:req.body.account_holder.toLowerCase(),
        account_number:req.body.account_number,
        photo:req.body.photo,

	})
}
const findData = async (customersName, req, search = {}, specificWhere = []) => {
    const where = {
        deleted_at: null,
    };

    if (Object.keys(search).length > 0) {
        where[Op.or] = [];
        for (const [key, value] of Object.entries(search)) {
            const src = {};
            src[key] = {
                [Op.like]: `%${value}%`,
            };
            where[Op.or].push(src);
        }
    }

    if (specificWhere.length > 0) {
        specificWhere.map((v) => Object.assign(where, v));
    }

    const cond = {
        where,
    };

    return await models.customers.findAndCountAll(cond).catch((ex) => {
        throw ex;
    });
}

const find = async(req) =>{
	return await models.customers.findOne({
		where: {
			id:req.params.id,
			deleted_at: null
		}
	})
}

const findOne = async(req) =>{
	return await models.customers.findOne({
		where: {
			name:req.body.name.toLowerCase(),
			deleted_at: null
		}
	})
}

const findCustomers = async(data) =>{
	return await models.customers.findOne({
		where: {
			name : data.customers.toLowerCase()
		},
		attributes:['id'],
		raw:true,
		plain:true
	})
}


module.exports = {
	insertBulk,
	getAllCustomers,
	deleteCustomers,
	updateById,
	createCustomers,
	findOne,
	findData,
	find,
	findCustomers
}