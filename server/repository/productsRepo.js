/* eslint-disable no-mixed-spaces-and-tabs */
'use strict'
const {models}= require('../models')
const {Op } = require('sequelize')

const insertBulk = async (data) => {
	return await models.products.bulkCreate(data)
}

const getAllProducts = async(data,limit =10, offset = 0)=>{
	const { product_name, product_code, unit_id, category_id } = data
    let whereConditions = {}
	let whereCategories = {}
	let whereUnits = {}
	// Menambahkan kondisi untuk product_name
    if (product_name) {
        whereConditions.product_name = { [Op.like]: `%${product_name}%` }
    }

    // Menambahkan kondisi untuk product_code
    if (product_code) {
        whereConditions.product_code = { [Op.eq]: product_code }
    }

    // Menambahkan kondisi untuk unit_id
    if (unit_id) {
        whereConditions['$units.id$'] = unit_id
    }

    // Menambahkan kondisi untuk category_id
    if (category_id) {
        whereConditions['$categories.id$'] = category_id
    }
	return await models.products.findAndCountAll({ 
		where: whereConditions,
		limit:parseInt(limit),
		offset: (offset <= 1) ? 0 : ((offset - 1) * parseFloat(limit)),
        include:[
			{
				model: models.categories,
				as:'categories',
				required:true,
				where: whereCategories,
				attributes:['id','category_name']
				
			},
            {
				model: models.units,
				as:'units',
				required:true,
				where: whereUnits,
				attributes:['id','unit_name']
				
			},
		],
		order:[['product_name', 'DESC']]
	 })
		.catch(ex => {
			throw ex
		})
}

const deleteProducts = async(req)=>{
	return await models.products.destroy({
		where:{id:req}
	})
}

const updateById = async(productId,productData)=>{
	return await models.products.update({		
        category_id: productData.category,
        unit_id: productData.unit,
		product_name: productData.product_name.toLowerCase(),
        product_code: productData.product_code,
        buying_price: productData.buying_price,
        selling_price: productData.selling_price,
        serial_number: productData.serial_number,
        image_name: productData.image_name,
        url: productData.url,
	}
	,{
		where:{id:productId}
	})
}

const createProducts = async(req)=>{
	return await models.products.create({
        category_id: req.category,
        unit_id: req.unit,
		product_name: req.product_name.toLowerCase(),
        product_code: req.product_code,
        buying_price: req.buying_price,
        serial_number: req.serial_number,
        selling_price: req.selling_price,
        image_name: req.image_name,
        url: req.url,
	})
}
const find = async(req) =>{
	return await models.products.findOne({
		where: {
			id:req.params.id,
			deleted_at: null
		}
	})
}

const findData = async(page, perpage,search = {},specificWhere = []) =>{
	let where = {
		deleted_at: null,
	}
	if (Object.keys(search).length > 0) {
		where[Op.or]= []
		for (const [key, value] of Object.entries(search)) {
		  const src = {}
		  src[key] = {
				[Op.like]: '%'+value+'%'
		  }
		  where[Op.or].push(src)
		}
  
		Object.assign(where)
	  }
	  if(specificWhere.length > 0) {
		specificWhere.map(v => Object.assign(where, v))
	  }
	  const cond = {
		where,
	  }
	  return await models.products.findAndCountAll(cond)
		.catch(ex => {
			throw ex
		})
}
const findOne = async(productData) =>{
	return await models.products.findOne({
		where: {product_name:productData.product_name.toLowerCase()},
		deleted_at:null
	})
}

const getProductById = async (req)=>{
	return await models.products.findOne({
		where: {
			id:req,
			deleted_at: null
		}
	})
}


const findProduct = async(data) =>{
	return await models.products.findOne({
		where: {
			name : data.products.toLowerCase()
		},
		attributes:['id'],
		raw:true,
		// plain:true
	})
}
const generateProductCode = async () => {
    const lastProducts = await models.products.findOne({
      order: [['id', 'DESC']], // Urutkan berdasarkan ID secara descending
    })
  
    let nextProductCode = 1

    if (lastProducts) {
        nextProductCode = parseInt(lastProducts.product_code.split('-')[1]) + 1
    }
  
    // Format nomor urut (contoh: "001")
    const formattedProductCode = String(nextProductCode).padStart(5, '0')
  
    // Gabungkan dengan format invoice (contoh: "inv-001")
    const newProductCode = `inv-${formattedProductCode}`;
    return newProductCode
  }
  
module.exports = {
	insertBulk,
	getAllProducts,
	deleteProducts,
	updateById,
	createProducts,
	findOne,
	find,
	findData,
	findProduct,
    generateProductCode,
	getProductById
}