'use strict'
const multer = require('multer');
// const uploadFile = require('../controller/uploadFile')
const unitsController = require('../controller/units')
const categoriesController = require('../controller/categories')
const customersController = require('../controller/customers')
const suppliersController = require('../controller/suppliers')
// const fileUploader = require('../util/fileUploader')
const authJwt = require('../middlewares/authJwt')
const usersController = require('../controller/users')
const productController = require('../controller/products')
const purchasesController = require('../controller/purchases')
const ordersController = require('../controller/orders')
// const invoiceController = require('../controller/invoice')
// const reportController = require('../controller/report')

const storage = multer.diskStorage({
	destination: './public/products',
	filename: function (req, file, cb) {
		const originalname = file.originalname;
		const sanitizedFilename = originalname.replace(/\s+/g, '-');
	 	cb(null, Date.now() + '-' + sanitizedFilename)
	}
})


// const upload = multer({ storage: storage })
const upload = multer({
	storage: storage,
	fileFilter: (req, file, cb) => {
	  if (
		file.mimetype == 'image/png' ||
		file.mimetype == 'image/jpg' ||
		file.mimetype == 'image/jpeg'
	  ) {
		cb(null, true);
	  } else {
		cb(null, false);
		return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
	  }
	},
})

module.exports = app =>{
	app.get('/api/health', (req, res) => {
		res.status(200).send({
			message: 'api is up and running',
		})
	})

	app.use(function(req, res, next) {
		res.header(
			'Access-Control-Allow-Headers',
			'x-access-token, Origin, Content-Type, Accept'
		)
		next()
	})

	app.post(
		'/api/auth/signup',[
			// verifySignUp.checkDuplicateUsernameOrEmail,
			// verifySignUp.checkRolesExisted
		],
		usersController.signup
	)
	
	app.post('/api/auth/signin', usersController.signin)

	// // api uploads
	// app.post('/api/file/upload',[authJwt.verifyToken],fileUploader.uploadExcelFile, uploadFile.uploadExcel

	// // api units
	app.get('/api/units', unitsController.getAll)
	app.delete('/api/units/:id',unitsController.deleteUnits)
	app.put('/api/units/:id',unitsController.updateUnits)
	app.post('/api/units/',unitsController.createUnits)


	// // api categories
	app.get('/api/categories', categoriesController.getAll)
	app.delete('/api/categories/:id',categoriesController.deleteCategories)
	app.put('/api/categories/:id',categoriesController.updateCategories)
	app.post('/api/categories/',categoriesController.createCategories)
	

	// // api customers
	app.get('/api/customers', customersController.getAll)
	app.delete('/api/customers/:id',customersController.deleteCustomers)
	app.put('/api/customers/:id',customersController.updateCustomers)
	app.post('/api/customers/',customersController.createCustomers)


	// // api suppliers
	app.get('/api/suppliers', suppliersController.getAll)
	app.delete('/api/suppliers/:id',suppliersController.deleteSuppliers)
	app.put('/api/suppliers/:id',suppliersController.updateSuppliers)
	app.post('/api/suppliers/',suppliersController.createSuppliers)

	// api user
	// app.get('/api/users', [authJwt.verifyToken],usersController.getUser)
	// app.get('/api/roles', [authJwt.verifyToken], usersController.getRole)
	// app.get('/api/users/:id', authController.getById)
	// app.put('/api/users/:id', [authJwt.verifyToken],usersController.updateUser)
	// app.delete('/api/users/:id', [authJwt.verifyToken],usersController.deleteUser)
	// app.put('/api/users/changepassword/:id', [authJwt.verifyToken],usersController.changePassword)
	app.get('/api/users', usersController.getUser)
	app.get('/api/roles', usersController.getRole)

	// api products
	app.get('/api/products',productController.getAll)
	app.delete('/api/products/:id', productController.deleteProduct)
	app.put('/api/products/:id', upload.single('url'), productController.updateProduct)
	// app.post('/api/products/', [authJwt.verifyToken],productController.createProduct)
	app.post('/api/products/', upload.single('url'), productController.createProduct)


	// api purchases
	app.get('/api/purchases',purchasesController.getAll)
	app.get('/api/purchases/:id',purchasesController.getID)
	app.delete('/api/purchases/:id',purchasesController.deletePurchases)
	app.put('/api/purchases/:id',purchasesController.updatePurchases)
	app.post('/api/purchases/', purchasesController.createPurchases)

	// api orders
	app.get('/api/orders',ordersController.getAll)
	app.get('/api/orders/:id', ordersController.getID)
	app.delete('/api/orders/:id', ordersController.deleteOrders)
	app.put('/api/orders/:id',ordersController.updateOrders)
	app.post('/api/orders/', ordersController.createOrders)



	// // api delivery delivery_orders
	// app.post('/api/delivery_orders', [authJwt.verifyToken],doController.createDO)
	// app.get('/api/delivery_orders', [authJwt.verifyToken],doController.getAll)
	// app.delete('/api/delivery_orders/:id',[authJwt.verifyToken], doController.deleteDO)
	// app.put('/api/delivery_orders/:id', [authJwt.verifyToken], doController.updateDO)
	// app.get('/api/delivery_orders/total',[authJwt.verifyToken], doController.getTotal)
	// app.get('/api/delivery_orders/chart', [authJwt.verifyToken],doController.getChart)
	// app.get('/api/delivery_orders/chart/customer',[authJwt.verifyToken], doController.getChartCustomer)


	// // api delivery invoice
	// app.post('/api/invoice', [authJwt.verifyToken],invoiceController.createInvoice)
	// app.get('/api/invoice', [authJwt.verifyToken],invoiceController.getAll)
	// app.delete('/api/invoice/:id', [authJwt.verifyToken],invoiceController.deleteInvoice)
	// app.put('/api/invoice/:id', [authJwt.verifyToken],invoiceController.updateInvoice)

	// // api report
	// app.get('/api/report', [authJwt.verifyToken],reportController.getAll)

	// // test user controller
	// app.get('/api/test/all', [authJwt.verifyToken],userController.allAccess)

	// app.get(
	// 	'/api/test/user',
	// 	[authJwt.verifyToken],
	// 	userController.userBoard
	// )

	// app.get(
	// 	'/api/test/mod',
	// 	[authJwt.verifyToken],
	// 	userController.moderatorBoard
	// )

	// app.get(
	// 	'/api/test/admin',
	// 	[authJwt.verifyToken, authJwt.isAdmin],
	// 	userController.adminBoard
	// )
}
