'use strict'
require('dotenv').config()
const express = require('express')
const multer = require('multer')
const app = express()
const cors = require('cors')
const bodyParser = require('body-parser')
const Routes = require('./server/routes')
const path = require('path')

const storage = multer.memoryStorage();  // Menggunakan memory storage, bisa disesuaikan dengan kebutuhan
const upload = multer({ storage: storage });

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.urlencoded({ extended: true }))
app.use('/public/products', express.static(path.join(__dirname, 'public', 'products')));

// app.use('/api/be/public/products', express.static(path.join(__dirname,'/public/products')))
// app.use(express.static(path.join(__dirname,'resources', 'assets', 'products')))
// app.use('/api/be/resources/assets/products', express.static(path.join(__dirname, 'resources', 'assets', 'products')));

process.env.FULL_FILEPATH = __dirname + '/'+process.env.FILEPATH

Routes(app)

// const db = require('./server/models')
const { models, db } = require('./server/models')
db.sequelize.sync({ force: false }).then(() => {
	console.log('Drop and re-sync db.')
})

// const db = require('./server/models')
// const Role = db.role;

// db.sequelize.sync()
  
const server = require('http').createServer(app) 
const PORT = process.env.PORT || process.env.APP_PORT || 3002
if (!module.parent) {
	server.listen(PORT, () => {
		console.log('Express Server Now Running. port:'+PORT)
	})
}
module.exports = app